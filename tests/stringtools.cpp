//
// Created by user on 2/27/20.
//

#include "catch2/catch.hpp"

#include "stringtools.hpp"

TEST_CASE( "StringTools toLower", "[DataWrapper]") {
    std::string str("My String");
    REQUIRE(StringTools::toLower(str) == "my string");
}

TEST_CASE( "StringTools split", "[DataWrapper]") {
    std::string str("test.test");
    auto toks = StringTools::split(str, ".");
    REQUIRE(toks[0] == "test");
    REQUIRE(toks[1] == "test");

    std::string str2("test");
    toks = StringTools::split(str, ".");
    REQUIRE(toks[0] == "test");
}
