//
// Created by user on 6/24/20.
//

#include "catch2/catch.hpp"

#include "bindpfn.hpp"

struct Foo {
    int add(int a, int b) { return a+b; }
    int number() const {return 5;}
};

TEST_CASE( "Bind a member function", "[bind_pfn]") {
    Foo f;
    auto bound = cyg::bind_pfn(f, &Foo::add);
    REQUIRE(bound(1,2) == 3);

    auto bound2 = cyg::bind_pfn(f, &Foo::number);
    REQUIRE(bound2() == 5);
}

