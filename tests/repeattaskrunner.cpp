//
// Created by user on 5/11/20.
//

#include <catch2/catch.hpp>
#include <atomic>
#include <threadpool.hpp>
#include <repeattaskrunner.hpp>
#include <iostream>

TEST_CASE("Repeat task runner", "[RepeatTaskRunner]") {
    std::atomic<int> acc(0);

    auto accumulator = [&acc]() {
        acc.store(acc.load() + 1);
    };

    cyg::ThreadPool tp;
    tp.spawn_workers(1);
    {
        cyg::RepeatTaskRunner runner(accumulator);
        runner.run(&tp);

        while (acc.load() < 10) {}

        REQUIRE(acc.load() >= 10);
    }
}
