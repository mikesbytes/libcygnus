//
// Created by user on 5/14/20.
//

#include <catch2/catch.hpp>
#include <threadpool.hpp>
#include <iostream>

class Thing {
public:
    int fn(int in) { return in + 5; }
};

TEST_CASE("Thread Pool Multithreading", "[ThreadPool]") {
    cyg::ThreadPool tp;
    tp.spawn_workers(2);

    std::atomic<int> foo = 0;
    auto bar = [&foo]() {
        ++foo;
    };

    for (int i = 0; i < 3; ++i) {
        tp.submit(bar);
    }

    REQUIRE(foo == 3);

    Thing thing;
    auto res = tp.submit(&Thing::fn, &thing, 2);
    REQUIRE(res.get() == 7);
}
