//
// Created by user on 7/13/20.
//

#include <libcygnus/uriutils.hpp>
#include "catch2/catch.hpp"

#include "uri.hpp"

TEST_CASE( "uri testing", "[URI]") {
    cyg::URI uri("http://user@test.com:1337//thing");
    REQUIRE(uri.scheme() == "http");
    REQUIRE(uri.host() == "test.com");
    REQUIRE(uri.user_info() == "user");
    REQUIRE(uri.port() == "1337");
    REQUIRE(uri.raw_path() == "thing");
    REQUIRE(uri.absolute() == false);

    REQUIRE(cyg::percent_encode_path("/test path/サンプ/to thing.flac") == "/test%20path/%E3%82%B5%E3%83%B3%E3%83%97/to%20thing.flac");
    REQUIRE(cyg::percent_decode_path("/test%20path/%E3%82%B5%E3%83%B3%E3%83%97/to%20thing.flac") == "/test path/サンプ/to thing.flac");

    auto uri2 = cyg::URIBuilder("cyg", "music/サンプルテキスト.flac").build();
    REQUIRE(uri2.path() == "music/サンプルテキスト.flac");
    REQUIRE(uri2.absolute() == false);
    REQUIRE(cyg::uri_from_source_path("test", "foo") == "cyg:/test/foo");
}

