//
// Created by user on 5/11/20.
//

#include <catch2/catch.hpp>
#include <network.hpp>
#include <threadpool.hpp>
#include <rpcdispatcher.hpp>
#include <iostream>
#include <repeattaskrunner.hpp>

bool invert(bool b) { return !b; }

TEST_CASE("RPC dispatch via network", "[NetRPCDispatch]") {
    /*
    auto tp = cyg::ThreadPool();
    tp.spawn_workers(2);

    cyg::Network net;
    std::atomic<bool> running(true);

    {
        cyg::RepeatTaskRunner process_in_runner(std::bind(&cyg::Network::process_in, &net));
        cyg::RepeatTaskRunner process_out_runner(std::bind(&cyg::Network::process_out, &net));

        auto new_con_fn = [&net](cyg::SocketPtr con) {};
        net.new_con_callback = new_con_fn;

        process_in_runner.run(&tp);
        process_out_runner.run(&tp);

        std::cout << "listen" << std::endl;
        auto sv = net.listen("", 6970, cyg::IP_TYPE::IPV6);
        std::cout << "connect" << std::endl;
        auto cl = net.connect_to("localhost", 6970);

        // rpc
        cyg::RPCDispatcher dp;
        dp.bind_method<bool, bool>("invert", invert, {"b"}, {std::nullopt});

        auto dispatch = [&dp](const cyg::SocketPtr &, const cyg::NetMessage &msg) {
            auto res = dp.parse_and_dispatch_message(cyg::from_net_message<std::string>(msg));
            REQUIRE(res.value()["result"] == false);
        };
        net.msg_callback = dispatch;

        nlohmann::json j = {{"jsonrpc", "2.0"},
                            {"method",  "invert"},
                            {"id",      0},
                            {"params",  {{"b", true}}}};
        net.queue_send(cl, cyg::to_net_message<std::string>(j.dump()));

        running.store(false);
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(1000));
     */
}
