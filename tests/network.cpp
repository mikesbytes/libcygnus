//
// Created by user on 3/30/20.
//

#include "catch2/catch.hpp"

#include <threadpool.hpp>
#include <network.hpp>
#include <iostream>

namespace cyg {

template<>
cyg::NetMessage to_net_message<std::string>(const std::string &str) {
    cyg::NetMessage msg;
    std::copy(str.begin(), str.end(), std::back_inserter(msg.data));
    return msg;
}

template<>
std::string from_net_message<std::string>(const NetMessage &msg) {
    return std::string(msg.data.begin(), msg.data.end());
}

} // namespace cyg

TEST_CASE("Listen and client sockets", "[Network]") {
    cyg::Network net;
    auto &tp = cyg::get_thread_pool("net");
    tp.spawn_workers(1);

    auto sv = net.listen("", 6969, cyg::IP_TYPE::IPV6);

    // send 1,2,3,4 when a new connection is made
    auto new_con_fn = [&net](cyg::SocketPtr con) {
        cyg::NetMessage msg;
        msg.data = {1, 2, 3, 4};
        net.queue_send(con, msg);
        //con->send(msg);
    };

    auto msg_fn = [](const cyg::SocketPtr &, const cyg::NetMessage &msg) {
        REQUIRE(msg.data.size() == 4);
        REQUIRE(msg.data[1] == 2);
    };

    net.new_con_callback = new_con_fn;
    net.msg_callback = msg_fn;


    auto fn = [&net]() {
        net.process_in();
    };
    tp.queueFn(fn);

    auto cl = net.connect_to("localhost", 6969);
    //std::cout << cl->get_fd() << "in! ";

    std::this_thread::sleep_for(std::chrono::milliseconds(200));
    net.process_out();

    std::this_thread::sleep_for(std::chrono::milliseconds(200));
    net.process_in();

    /*
    auto rec = cl->recv<cyg::NetMessage>();
    REQUIRE(rec.data.size() == 4);
    REQUIRE(rec.data[1] == 2);
     */
}

TEST_CASE("NetMessage conversion", "[NetMessage]") {
    auto msg = cyg::to_net_message(std::string("hello"));
    REQUIRE(msg.data.size() == 5);
    REQUIRE(cyg::from_net_message<std::string>(msg) == "hello");
}
