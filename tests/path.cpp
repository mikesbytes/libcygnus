//
// Created by user on 7/13/20.
//

#include "catch2/catch.hpp"

#include "path.hpp"

TEST_CASE( "path testing", "[Path]") {
    cyg::Path p("/foo/bar/baz//");
    REQUIRE(p.is_absolute());
    REQUIRE(p[0] == "foo");
    REQUIRE(p[2] == "baz");
    REQUIRE(p[3] == "");
    REQUIRE(p.element_count() == 3);
    REQUIRE(p.trim_slashes().str() == "foo/bar/baz");
    REQUIRE(p.element_range_str(1, 2) == "bar/baz");
    REQUIRE(p.element_range_str(1) == "bar/baz");

    cyg::Path p2("foo/bar/baz");
    REQUIRE(!p2.is_absolute());
    REQUIRE(p2[0] == "foo");
    REQUIRE(p2[2] == "baz");
    REQUIRE(p2[3] == "");
    REQUIRE(p2.element_range_str(0,2) == "foo/bar");

    cyg::Path p3 = p + p2;
    REQUIRE(p3.str() == "/foo/bar/baz/foo/bar/baz");
}

