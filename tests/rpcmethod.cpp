//
// Created by user on 3/9/20.
//

#include "catch2/catch.hpp"

#include "rpcmethod.hpp"
#include "rpcwrapper.hpp"
#include "rpcdispatcher.hpp"

bool fn_1(bool foo) {
    return !foo;
}

bool fn_2(nlohmann::json j) {
    return j.at("foo");
}

bool check = false;

void fn_3(bool foo) {
    check = foo;
}

bool fn_4() {
    return true;
}


nlohmann::json fn_5(bool foo) {
    return nlohmann::json({{"foo",!foo}});
}
bool fn_6(std::optional<int> x) {
    return x.has_value();
}

auto rpcm_1 = cyg::RPCMethod<bool,bool>(
        fn_1,
        {"foo"},
        std::tuple(std::nullopt)
);

auto rpcm_6 = cyg::RPCMethod<bool, std::optional<int>>(
        fn_6,
        {"x"},
        {std::nullopt});

TEST_CASE( "Standard RPC method", "[RPCMethod]") {
    nlohmann::json args = {{"foo", true}};
    nlohmann::json args2 = {};
    nlohmann::json args3 = {{"foo", "not a bool"}};

    REQUIRE(rpcm_1({{"foo",true}}) == false);

    auto rpcm_2 = cyg::RPCMethod<bool,nlohmann::json>(
            fn_2
    );

    REQUIRE(rpcm_2(args) == true);

    auto rpcm_3 = cyg::RPCMethod<void,bool>(fn_3, {"foo"}, {std::nullopt});
    rpcm_3(args);
    REQUIRE(check == true);

    REQUIRE_THROWS_WITH(rpcm_1(args2), Catch::Contains("Missing"));
    REQUIRE_THROWS_WITH(rpcm_1(args3), Catch::Contains("Invalid parameter:"));

    auto rpcm_5 = cyg::RPCMethod<nlohmann::json, bool>(fn_5, {"foo"}, {std::nullopt});
    rpcm_5(args);

    REQUIRE(rpcm_6({{"x",5}}));
}

TEST_CASE( "RPC Wrapper", "[RPCWrapper]") {
    cyg::RPCWrapper wrapper(fn_4);
    REQUIRE(wrapper({})["result"] == true);

    cyg::RPCWrapper wrapper2(rpcm_1);
    REQUIRE(wrapper2({})["error"]["code"] == -32602);
    REQUIRE(wrapper2({{"foo",true}})["result"] == false);
}

TEST_CASE( "RPC Dispatcher", "[RPCDispatcher]") {
    cyg::RPCDispatcher dp;
    REQUIRE(dp.dispatch_message({{"jsonrpc","2.0"}}).value()["error"]["code"] == -32600);
    REQUIRE(dp.dispatch_message({{"jsonrpc","2.0"},{"method","fn_4"}}).value()["error"]["code"] == -32601);

    /*
    auto fn_4_wrap = std::unique_ptr<cyg::RPCWrapperBase>(new cyg::RPCWrapper(fn_4));
    dp.bind_wrapper("fn_4", std::move(fn_4_wrap));
     */
    dp.bind_method("fn_4", fn_4);

    REQUIRE(dp.dispatch_message({{"jsonrpc","2.0"},{"method","fn_4"}}) == std::nullopt);
    REQUIRE(dp.dispatch_message({{"jsonrpc","2.0"},{"method","fn_4"},{"id",0}}).value()["result"] == true);


    /*
    auto test = std::unique_ptr<cyg::RPCWrapperBase>(new cyg::RPCWrapper(cyg::RPCMethod<bool,bool>(
            fn_1,
            {"foo"},
            std::tuple(std::nullopt)
    )));
    dp.bind_wrapper("foo", std::move(test));
     */

    dp.bind_method<bool,bool>("foo", fn_1, {"foo"}, {std::nullopt});
    auto res = dp.dispatch_message({{"jsonrpc","2.0"},{"method","foo"},{"params",{{"foo",true}}},{"id",0}});
    REQUIRE(res.value()["result"] == false);
}
