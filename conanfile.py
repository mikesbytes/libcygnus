# noinspection PyInterpreter
from conans import ConanFile, CMake, tools
import re, os


class LibcygnusConan(ConanFile):
    name = "libcygnus"
    license = "None"
    author = "mikesbytes mail@mikesbytes.org"
    url = "https://gitlab.com/mikesbytes/libcygnus"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = {"shared": False}
    generators = "cmake"
    exports_sources = "*"

    def set_version(self):
        content = tools.load(os.path.join(self.recipe_folder, "CMakeLists.txt"))
        version = re.search("project\(libcygnus VERSION (.*)\)", content).group(1)
        self.version = version.strip()

    def build_requirements(self):
        self.build_requires("boost/1.72.0")
        self.build_requires("nlohmann_json/3.7.3")
        self.build_requires("concurrentqueue/git")
        self.build_requires("catch2/2.11.0")
        self.build_requires("fmt/6.1.2")
        self.build_requires("toml11/3.1.0")
        self.build_requires("uriparser/0.9.4")


    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()

        # Explicit way:
        # self.run('cmake %s/hello %s'
        #          % (self.source_folder, cmake.command_line))
        # self.run("cmake --build . %s" % cmake.build_config)

    def package(self):
        self.copy("*.hpp", dst="include", src="include")
        self.copy("*hello.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.dylib", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["cygnus"]
