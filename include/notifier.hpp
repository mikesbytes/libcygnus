//
// Created by user on 12/28/19.
//

#ifndef LIBCYGNUS_NOTIFIER_HPP
#define LIBCYGNUS_NOTIFIER_HPP

#include <mutex>
#include <condition_variable>

/// thread safe object for waiting on something to happen
class Notifier {
public:
    Notifier();

    void wait();
    void notify();

private:
    std::mutex notify_mtx_;
    std::condition_variable notify_cv_;
    bool notify_val_;
};


#endif //LIBCYGNUS_NOTIFIER_HPP
