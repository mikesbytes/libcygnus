//
// Created by mikesbytes on 3/22/20.
// Credit to Léo Masson for helping work out some UB with the to_net_message functions
//
// TODO: fully clean up sockets after disconnect
//

#ifndef LIBCYGNUS_NETWORK_HPP
#define LIBCYGNUS_NETWORK_HPP

#include <cstddef>
#include <string>
#include <map>

#include <blockingconcurrentqueue.h>
#include <mutex>
#include <shared_mutex>

namespace cyg {

enum class IP_TYPE {
    IPV4,
    IPV6,
    UNSPEC
};

class Socket;
class ListenSocket;

typedef std::shared_ptr<Socket> SocketPtr;
typedef std::shared_ptr<ListenSocket> ListenSocketPtr;

struct NetworkException : public std::runtime_error {
    using std::runtime_error::runtime_error;
};


struct NetMessage {
    std::vector<uint8_t> data;
};

class Network {
public:
    Network();
    ~Network();

    SocketPtr connect_to(const std::string &host, uint16_t port, IP_TYPE ip_type = IP_TYPE::UNSPEC);
    ListenSocketPtr listen(const std::string &host, uint16_t port, IP_TYPE ip_type = IP_TYPE::UNSPEC);

    void process_in();
    void process_out();

    void queue_send(SocketPtr sock, const NetMessage &msg);

    std::function<void(SocketPtr, const NetMessage &)> msg_callback;
    std::function<void(SocketPtr)> new_con_callback;
    std::function<void(SocketPtr)> disconnect_callback;
private:
    void add_pfd(int fd);
    void remove_pfd(int fd);

    // thjs is a void pointer to avoid bringing in the include for poll_fd
    // TODO: add mutex for this
    std::shared_mutex fds_mutex;
    void* poll_fds_;
    uint32_t poll_fds_size_;
    uint32_t poll_fds_used_;

    std::mutex sockets_mutex_;
    std::map<int, SocketPtr> sockets_;

    std::mutex listen_sockets_mutex_;
    std::map<int, ListenSocketPtr> listen_sockets_;

    moodycamel::BlockingConcurrentQueue<std::pair<SocketPtr, NetMessage>> out_queue_;
};

template<typename T>
NetMessage to_net_message(const T& msg);

template<typename T>
T from_net_message(const NetMessage& msg);

class Socket : public std::enable_shared_from_this<Socket> {
    friend class Network;
    friend class ListenSocket;
public:
    Socket();
    Socket(Socket &&socket);
    ~Socket();

    [[nodiscard]] const std::string & get_host() const { return host_; }

    template<typename T>
    void send(const T& msg) {
        send(to_net_message(msg));
    }

    template<typename T>
    void queue_send(const T& msg) {
        queue_send(to_net_message(msg));
    }

    void send(const NetMessage &msg);
    void queue_send(const NetMessage &msg);

    template<typename T>
    T recv() {
        if constexpr(std::is_same_v<T, NetMessage>) {
            return recv_msg();
        } else {
            return from_net_message<T>(recv_msg());
        }
    }

    [[nodiscard]] int get_fd() const {return fd_;}
    [[nodiscard]] Network *get_network() const {return network_;}
    [[nodiscard]] const std::string &get_host() { return host_; }
    [[nodiscard]] uint16_t get_port() { return port_; }
    [[nodiscard]] bool get_connected() { return connected_.load(); }

    Socket &operator=(Socket &&socket);

protected:
    Network *network_;
    NetMessage recv_msg();
    std::atomic<bool> connected_;

    int fd_;
    std::string host_;
    uint16_t port_;

private:
    Socket(const Socket&); // disable copy constructor
    Socket &operator=(const Socket &socket); // disable assignment operator
};

class ListenSocket : public Socket {
public:
    Socket accept();
protected:

};

} // namespace cyg

#endif //LIBCYGNUS_NETWORK_HPP
