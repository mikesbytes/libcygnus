//
// Created by user on 6/13/19.
//

#ifndef CYGNUS_THREADPOOL_HPP
#define CYGNUS_THREADPOOL_HPP

#include <functional>
#include <vector>
#include <blockingconcurrentqueue.h>
#include <future>

namespace cyg {


class ThreadPool {
private:
    class ThreadTaskBase {
    public:
        ThreadTaskBase() = default;
        virtual ~ThreadTaskBase() = default;
        ThreadTaskBase(const ThreadTaskBase &) = delete;
        ThreadTaskBase &operator=(const ThreadTaskBase &) = delete;
        ThreadTaskBase(ThreadTaskBase &&other) = default;
        ThreadTaskBase &operator=(ThreadTaskBase &&) = default;

        virtual void execute() = 0;
    };

    template <typename Fn>
    class ThreadTask : public ThreadTaskBase {
    public:
        ThreadTask(Fn &&fn) :
            fn_{std::move(fn)}
        {}

        ~ThreadTask() override = default;
        ThreadTask(const ThreadTask &) = delete;
        ThreadTask &operator=(const ThreadTask &) = delete;
        ThreadTask(ThreadTask &&) = default;
        ThreadTask &operator=(ThreadTask &&) = default;

        void execute() override {
            fn_();
        }
    private:
        Fn fn_;
    };

public:
    template <typename T>
    class TaskFuture {
    public:
        TaskFuture() = default;

        TaskFuture(std::future<T> &&future) :
            future_{std::move(future)},
            destruct_early_(false)
        {}

        TaskFuture(const TaskFuture &) = delete;
        TaskFuture &operator=(const TaskFuture &) = delete;
        TaskFuture(TaskFuture &&other) :
            future_{std::move(other.future_)},
            destruct_early_{other.destruct_early_.load()}
        {}

        TaskFuture &operator=(TaskFuture &&other) {
            future_ = std::move(other.future_);
            destruct_early_ = other.destruct_early_.load();
            return *this;
        }

        ~TaskFuture() {
            if (!destruct_early_ && future_.valid()) {
                future_.get();
            }
        }

        auto get() {
            return future_.get();
        }

        /// Allow this Future to destruct without waiting for a result
        void destruct_early() {
            destruct_early_ = true;
        }

    private:
        std::future<T> future_;
        std::atomic_bool destruct_early_;
    };

    ThreadPool();
    ThreadPool(const ThreadPool &) = delete;
    ThreadPool &operator=(const ThreadPool &) = delete;

    ~ThreadPool() {
        destroy_();
    }

    void spawn_workers(int worker_count);

    template <typename Fn, typename... Args>
    auto submit(Fn &&fn, Args &&... args) {
        auto bound_task = std::bind(std::forward<Fn>(fn), std::forward<Args>(args)...);
        using ResultType = std::result_of_t<decltype(bound_task)()>;
        using PackagedTask = std::packaged_task<ResultType()>;
        using TaskType = ThreadTask<PackagedTask>;

        PackagedTask task{std::move(bound_task)};
        TaskFuture<ResultType> result{task.get_future()};
        work_queue_.enqueue(std::make_unique<TaskType>(std::move(task)));
        return std::move(result);
    }

private:
    void worker_();
    void destroy_();

    moodycamel::BlockingConcurrentQueue<std::unique_ptr<ThreadTaskBase>> work_queue_;
    std::vector<std::thread> workers_;
    std::atomic_bool done_;
};

ThreadPool &get_thread_pool(const std::string &key);

}


#endif //CYGNUS_THREADPOOL_HPP
