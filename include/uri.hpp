//
// Created by user on 7/13/20.
// uri examples:
// cyg:///music/Queen/Greatest Hits/We Will Rock You.flac
// cyg://remoteserver.net/music/artist/album/song.flac
//

#ifndef LIBCYGNUS_URI_HPP
#define LIBCYGNUS_URI_HPP

#include <string>
#include <optional>
#include <uriparser/Uri.h>
#include <stdexcept>

namespace cyg {

struct URIException : public std::runtime_error {
    using std::runtime_error::runtime_error;
};

std::string percent_encode_path(std::string_view input);
std::string percent_decode_path(std::string_view input);

class URI;

class URIBuilder {
public:
    URIBuilder(std::string_view scheme, std::string_view path);

    /// mark the path as already percent encoded
    URIBuilder &encoded_path();
    URI build();
private:
    bool encoded_path_;
    std::string_view scheme_;
    std::string_view authority_;
    std::string_view path_;
};

class URI {
public:
    /// Construct a URI from a RFC 3986 compliant string. Throws URIException if parsing fails.
    URI(const std::string &uri);
    ~URI();

    std::string_view scheme();
    std::string_view host();
    std::string_view user_info();
    std::string_view port();

    /// percent encoded path
    std::string_view raw_path();

    /// regular decoded path, has leading slash if absolute
    std::string path();

    bool absolute() const;
private:
    std::string uri_data_;
    std::string uri_path_;
    UriUriA uri_;
};

}

#endif //LIBCYGNUS_URI_HPP
