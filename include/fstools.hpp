//
// Created by user on 12/8/19.
//

#ifndef CYGNUS_FSTOOLS_HPP
#define CYGNUS_FSTOOLS_HPP

#include <vector>
#include <string>

namespace fstools {

    std::string env(const std::string &v_name);

    /// returns first file/dir in path_list that exists, otherwise empty string
    std::string fs_fallback(const std::vector<std::string> &path_list);

}


#endif //CYGNUS_FSTOOLS_HPP
