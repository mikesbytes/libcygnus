//
// Created by user on 5/11/20.
//

#ifndef LIBCYGNUS_REPEATTASKRUNNER_HPP
#define LIBCYGNUS_REPEATTASKRUNNER_HPP

#include <functional>

#include "threadpool.hpp"

namespace cyg {

template <typename Fn>
class RepeatTaskRunner {
public:
    RepeatTaskRunner(Fn fn) :
        running_(false),
        fn_(std::move(fn))
    {}

    ~RepeatTaskRunner() {
        future_.destruct_early();
        running_ = false;
    }

    void run(ThreadPool *pool) {
        running_ = true;

        future_ = std::move(pool->submit(&RepeatTaskRunner::runner_, this, pool));
    }

private:
    Fn fn_;
    std::atomic<bool> running_;
    ThreadPool::TaskFuture<void> future_;

    void runner_(ThreadPool *pool) {
        if (!running_) return;

        fn_();
        future_.destruct_early(); // allow old future to destruct gracefully without blocking
        future_ = std::move(pool->submit(&RepeatTaskRunner::runner_, this, pool));
    }
};

}


#endif //LIBCYGNUS_REPEATTASKRUNNER_HPP
