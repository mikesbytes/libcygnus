//
// Created by user on 6/24/20.
// Credit to Léo Masson for the original functions
//

#ifndef LIBCYGNUS_BINDPFN_HPP
#define LIBCYGNUS_BINDPFN_HPP

#include <utility>

namespace cyg {

template<typename T, typename R, typename... Args>
auto bind_pfn(T &instance, R(T::* pfn)(Args...)) {
    return [&instance, pfn](Args... args) -> decltype(auto) {
        return (instance.*pfn)(std::forward<Args>(args)...);
    };
}

template<typename T, typename R, typename... Args>
auto bind_pfn(T const &instance, R(T::* pfn)(Args...) const) {
    return [&instance, pfn](Args... args) -> decltype(auto) {
        return (instance.*pfn)(std::forward<Args>(args)...);
    };
}

} // namespace cyg

#endif //LIBCYGNUS_BINDPFN_HPP
