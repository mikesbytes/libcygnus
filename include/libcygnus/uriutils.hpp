//
// Created by user on 9/1/20.
//

#ifndef LIBCYGNUS_URIUTILS_HPP
#define LIBCYGNUS_URIUTILS_HPP

#include <string>

namespace cyg {

std::string uri_from_source_path(const std::string &source, const std::string &path);

}


#endif //LIBCYGNUS_URIUTILS_HPP
