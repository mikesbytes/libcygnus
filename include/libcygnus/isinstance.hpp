//
// Created by user on 8/28/20.
//

#ifndef LIBCYGNUS_ISINSTANCE_HPP
#define LIBCYGNUS_ISINSTANCE_HPP

#include <type_traits>

namespace cyg {

namespace {

template<typename, template<typename...> typename>
struct is_instance_impl : public std::false_type {};

template<template<typename...> typename U, typename...Ts>
struct is_instance_impl<U<Ts...>, U> : public std::true_type {};

} // namespace

template<typename T, template<typename ...> typename U>
using is_instance = is_instance_impl<std::decay_t<T>, U>;

} // namespace cyg

#endif //LIBCYGNUS_ISINSTANCE_HPP
