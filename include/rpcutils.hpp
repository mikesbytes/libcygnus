//
// Created by user on 6/30/20.
//

#ifndef LIBCYGNUS_RPCUTILS_HPP
#define LIBCYGNUS_RPCUTILS_HPP

#include <nlohmann/json.hpp>

namespace cyg {

template <typename T = nullptr_t>
nlohmann::json procedure_call_template(const std::string &method, const nlohmann::json &params, std::optional<T> id = std::nullopt) {
    nlohmann::json j = {{"jsonrpc", "2.0"}, {"method", method}, {"params", params}};
    if (id.has_value()) {
        j["id"] = id.value();
    }
    return j;
}

}

#endif //LIBCYGNUS_RPCUTILS_HPP
