//
// Created by user on 12/8/19.
//

#ifndef CYGNUS_STRINGTOOLS_HPP
#define CYGNUS_STRINGTOOLS_HPP

#include <string>
#include <vector>

namespace StringTools {
    std::string toLower(const std::string &in);

    std::vector<std::string> split(const std::string &str, const std::string &delim);
}

#endif //CYGNUS_STRINGTOOLS_HPP
