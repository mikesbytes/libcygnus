//
// Created by user on 3/18/20.
//

#ifndef LIBCYGNUS_RPCWRAPPER_HPP
#define LIBCYGNUS_RPCWRAPPER_HPP

#include <type_traits>
#include <nlohmann/json.hpp>
#include "rpcmethod.hpp"

namespace cyg {

class RPCWrapperBase {
public:
    virtual nlohmann::json operator() (const nlohmann::json& params) = 0;
    virtual void load_response (nlohmann::json& response, const nlohmann::json& params) = 0;
};

template <typename T>
class RPCWrapper : public RPCWrapperBase {
    typedef nlohmann::json json;
public:
    RPCWrapper(T invokable) :
        invokable_(invokable)
    {
        // ensure T accepts either json or no args
        static_assert(std::is_invocable<T>::value ||
                      std::is_invocable<T, const json &>::value);
    }

    json operator() (const json &params) override {
        json j = {};
        load_response(j, params);
        return j;
    }

    void load_response (json &response, const json &params) override {
        try {
            if constexpr(std::is_invocable<T>::value) {
                if constexpr (std::is_same_v<typename std::invoke_result_t<T>, void>) {
                    invokable_();
                    response["result"] = "ok";
                } else {
                    // handle optional return type
                    if constexpr (cyg::is_instance<typename std::invoke_result_t<T>, std::optional>{}) {
                        auto ret = invokable_();
                        if (ret.has_value()) {
                            response["result"] = ret.value();
                        } else {
                            response["result"] = nullptr;
                        }
                    } else {
                        response["result"] = invokable_();
                    }
                }
            } else {
                if constexpr (std::is_same_v<typename std::invoke_result_t<T, const json &>, void>) {
                    invokable_(params);
                    response["result"] = "ok";
                } else {
                    // handle optional return type
                    if constexpr (cyg::is_instance<typename std::invoke_result_t<T, const json &>, std::optional>{}) {
                        auto ret = invokable_(params);
                        if (ret.has_value()) {
                            response["result"] = ret.value();
                        } else {
                            response["result"] = nullptr;
                        }
                    } else {
                        response["result"] = invokable_(params);
                    }
                }
            }
        } catch (const RPCInvalidParamsException &e) {
            // catch invalid parameters
            response["error"] =
                    {{"code", -32602},
                     {"message", e.what()}};
        } catch (const std::exception &e) {
            // catch other errors
            response["error"] =
                    {{"code", -32603},
                     {"message", e.what()}};
        }
    }
private:
    T invokable_;
};

}

#endif //LIBCYGNUS_RPCWRAPPER_HPP
