//
// Created by user on 3/18/20.
//

#ifndef LIBCYGNUS_RPCDISPATCHER_HPP
#define LIBCYGNUS_RPCDISPATCHER_HPP

#include "rpcwrapper.hpp"
#include "network.hpp"

#include <nlohmann/json.hpp>
#include <map>

namespace cyg {
class RPCDispatcher {
    typedef nlohmann::json json;

    template<typename T>
    using nondeduced_t = std::common_type_t<T>;
public:
    std::optional<json> dispatch_message(const json &j, SocketPtr = nullptr);
    std::optional<json> parse_and_dispatch_message(const std::string &msg, SocketPtr = nullptr);

    template<typename T>
    void bind_method(const std::string &key, T method) {
        std::unique_ptr<RPCWrapperBase> wrapper(new RPCWrapper(method));
        bind_wrapper(key, std::move(wrapper));
    }

    template<typename T, typename... Args>
    void bind_method(const std::string &key,
                      nondeduced_t<std::function<T(Args...)>> fn,
                      std::array<std::string, sizeof...(Args)> param_names = {},
                      std::tuple<std::optional<typename std::decay<Args>::type>...> param_defaults =
                      std::tuple<std::optional<typename std::decay<Args>::type>...>())
    {
        auto wrapper = std::unique_ptr<RPCWrapperBase>(new RPCWrapper(
                RPCMethod<T,Args...>(fn, param_names, param_defaults)));
        bind_wrapper(key, std::move(wrapper));
    }

    void bind_wrapper(const std::string &key, std::unique_ptr<RPCWrapperBase> method);
    void bind_raw(const std::string &key, std::function<std::optional<json>(SocketPtr, const json&)> fn);

    void process(cyg::SocketPtr sock, const cyg::NetMessage &msg);
private:
    std::map<std::string, std::unique_ptr<RPCWrapperBase>> methods_;
    std::map<std::string, std::function<std::optional<json>(SocketPtr, const json&)>> raw_methods_;
};

}

#endif //LIBCYGNUS_RPCDISPATCHER_HPP
