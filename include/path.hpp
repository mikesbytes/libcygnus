//
// Created by user on 7/13/20.
//

#ifndef LIBCYGNUS_PATH_HPP
#define LIBCYGNUS_PATH_HPP

#include <string>
#include <vector>
#include <limits>

namespace cyg {

class Path {
public:
    static const size_t npos = std::numeric_limits<size_t>::max();

    Path(const std::string &path_string);
    const std::string &str() const;

    bool is_absolute() const;
    size_t element_count() const;

    std::vector<std::string> elements() const;

    std::string_view element_range_str(size_t start, size_t count = npos) const;

    /// returns the same path sans any leading or trailing slashes
    Path trim_slashes() const;


    /// get a path section
    std::string_view operator[](std::size_t index) const;

    /// concat ops
    Path &operator+=(const Path &rhs);
private:
    std::string str_trim_slashes() const;
    std::string path_data_;
};

inline Path operator+(Path lhs, const Path &rhs) {
    lhs += rhs;
    return lhs;
}

}


#endif //LIBCYGNUS_PATH_HPP
