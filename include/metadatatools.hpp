//
// Created by user on 12/12/19.
//

#ifndef CYGNUS_METADATATOOLS_HPP
#define CYGNUS_METADATATOOLS_HPP

#include "trackmetadata.hpp"
#include "nlohmann/json.hpp"

void to_json(nlohmann::json &j, const TrackMetadata &m);
void from_json(const nlohmann::json &j, TrackMetadata &m);


#endif //CYGNUS_METADATATOOLS_HPP
