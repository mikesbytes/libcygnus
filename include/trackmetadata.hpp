//
// Created by user on 12/12/19.
//

#ifndef CYGNUS_TRACKMETADATA_HPP
#define CYGNUS_TRACKMETADATA_HPP

#include <string>
#include <optional>

struct TrackMetadata{
    std::optional<std::string> filename;
    std::optional<std::string> title;
    std::optional<std::string> artist;
    std::optional<std::string> album;
    std::optional<std::string> album_artist;
    std::optional<unsigned> track;
    std::optional<int> length;
    std::optional<int> year;
    std::optional<std::string> genre;
};

#endif //CYGNUS_TRACKMETADATA_HPP
