//
// Created by user on 9/1/20.
//

#include <libcygnus/uriutils.hpp>
#include <path.hpp>
#include <uri.hpp>
#include <fmt/format.h>

std::string cyg::uri_from_source_path(const std::string &source, const std::string &path) {
    std::string processed_path = (cyg::Path(source).trim_slashes() + cyg::Path(path)).str();
    processed_path = cyg::percent_encode_path(processed_path);
    return fmt::format("cyg:/{}", processed_path);
}

