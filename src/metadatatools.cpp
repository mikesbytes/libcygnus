//
// Created by user on 12/12/19.
//

#include "metadatatools.hpp"

void to_json(nlohmann::json &j, const TrackMetadata &m) {
    if (m.artist.has_value())
        j["artist"] = m.artist.value();
    if (m.album.has_value())
        j["album"] = m.album.value();
    if (m.title.has_value())
        j["title"] = m.title.value();
    if (m.track.has_value())
        j["track"] = m.track.value();
    if (m.length.has_value())
        j["length"] = m.length.value();
    if (m.album_artist.has_value())
        j["album-artist"] = m.album_artist.value();
}

void from_json(const nlohmann::json &j, TrackMetadata &m) {
    if (j.contains("artist"))
        m.artist = j.at("artist");
    if (j.contains("album"))
        m.album = j.at("album");
    if (j.contains("title"))
        m.title = j.at("title");
    if (j.contains("track"))
        m.track = j.at("track");
    if (j.contains("length"))
        m.length = j.at("length");
    if (j.contains("album-artist"))
        m.album_artist = j.at("album-artist");
}
