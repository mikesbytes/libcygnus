//
// Created by user on 12/8/19.
//

#include <algorithm>
#include "stringtools.hpp"

std::string StringTools::toLower(const std::string &in) {
    std::string out = in;
    std::transform(out.begin(), out.end(), out.begin(),
            [](unsigned char c){return std::tolower(c); });
    return out;
}

std::vector<std::string> StringTools::split(const std::string &str, const std::string &delim) {
    std::vector<std::string> tokens;
    std::string chopped = str;
    size_t pos;
    do {
        pos = chopped.find(delim);
        tokens.push_back(chopped.substr(0,pos));
        chopped.erase(0, pos + delim.length());
    } while (pos != std::string::npos && !chopped.empty());

    return tokens;
}
