//
// Created by user on 7/13/20.
//

#include "path.hpp"
#include "stringtools.hpp"

namespace cyg {


Path::Path(const std::string &path_string) :
path_data_(path_string)
{
}

bool Path::is_absolute() const {
    return (!path_data_.empty() && path_data_[0] == '/');
}

std::string_view Path::operator[](std::size_t index) const {
    auto start = path_data_.begin() + path_data_.find_first_not_of('/');
    auto i = start;
    size_t count = 0;

    while (true) {
        while (*i != '/' && i != path_data_.end()) ++i;

        if (count == index) {
            return std::string_view(&*start, i - start);
        } else if (i == path_data_.end()) {
            // TODO: throw exception
            return "";
        }

        while (*i == '/') ++i;
        start = i;
        ++count;
    }
}

size_t Path::element_count() const {
    auto cut = str_trim_slashes();
    if (cut.empty()) return 0;

    size_t count = 0;
    for (size_t i = 1; i < cut.size() - 1; ++i) {
        if (cut[i] == '/') ++count;
    }

    return count + 1;
}

Path Path::trim_slashes() const {
    return Path(str_trim_slashes());
}

const std::string &Path::str() const {
    return path_data_;
}

std::string Path::str_trim_slashes() const {
    size_t start = path_data_.find_first_not_of('/');
    size_t end = path_data_.find_last_not_of('/');
    if (start == std::string::npos || end == std::string::npos)
        return "";
    return path_data_.substr(start, end - start + 1);
}

std::vector<std::string> Path::elements() const {
    return StringTools::split(str_trim_slashes(), "/");
}

std::string_view Path::element_range_str(size_t start, size_t count) const {
    if (count == npos) {
        count = element_count() - start;
    }

    auto start_it = path_data_.begin() + path_data_.find_first_not_of('/');
    auto i = start_it;
    size_t cur_element = 0;

    while (true) {
        if(cur_element == start) {
            start_it = i;
        }

        while (*i != '/' && i != path_data_.end()) ++i;

        if (cur_element == start + count - 1) {
            return std::string_view(&*start_it, i - start_it);
        } else if (i == path_data_.end()) {
            // TODO: throw exception
            return "";
        }

        while (*i == '/') ++i;
        ++cur_element;
    }
}

Path &Path::operator+=(const Path &rhs) {
    auto lhs_end = path_data_.find_last_not_of('/');
    if (lhs_end == std::string::npos) path_data_ = rhs.path_data_;

    auto rhs_start = rhs.path_data_.find_first_not_of('/');
    if (rhs_start != std::string::npos) {
        path_data_ = path_data_.substr(0, lhs_end + 1) + '/' +
                rhs.path_data_.substr(rhs_start, std::string::npos);
    }

    return *this;
}


}