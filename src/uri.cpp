//
// Created by user on 7/13/20.
//

#include "uri.hpp"

#include <fmt/format.h>
#include <charconv>

namespace cyg {

URI::URI(const std::string &uri) {
    uri_data_ = uri;
    const char *error;
    if (uriParseSingleUriA(&uri_, uri_data_.c_str(), &error) != URI_SUCCESS) {
        throw URIException(fmt::format("URI Parsing failed: {}", error));
    }
}

URI::~URI() {
    uriFreeUriMembersA(&uri_);

}

std::string_view URI::scheme() {
    return std::string_view(uri_.scheme.first,
                            uri_.scheme.afterLast - uri_.scheme.first);
}

std::string_view URI::host() {
    return std::string_view(uri_.hostText.first,
                            uri_.hostText.afterLast - uri_.hostText.first);
}

std::string_view URI::user_info() {
    return std::string_view(uri_.userInfo.first,
                            uri_.userInfo.afterLast - uri_.userInfo.first);
}

std::string_view URI::port() {
    return std::string_view(uri_.portText.first,
                            uri_.portText.afterLast - uri_.portText.first);
}

std::string_view URI::raw_path() {
    if (uri_.pathHead == nullptr || uri_.pathTail == nullptr) {
        throw URIException("Path is null");
    }
    auto head = uri_.pathHead;
    while (head->text.first - head->text.afterLast == 0) {
        if (head->next == nullptr) throw URIException("Invalid URI Path");
        head = head->next;
    }
    const char * start = head->text.first;
    const char * end = uri_.pathTail->text.afterLast;
    return std::string_view(start, end - start);
}

bool URI::absolute() const {
    return (uri_.absolutePath == URI_TRUE);
}

std::string URI::path() {
    std::string decoded_path;
    if (absolute()) decoded_path = "/";
    decoded_path += percent_decode_path(raw_path());
    return decoded_path;
}

std::string percent_encode_path(std::string_view input) {
    std::string output;
    output.reserve(input.size() * 3);
    for (char i : input) {
        if (
                (i >= 48 && i <= 57) || //digit
                (i >= 65 && i <= 90) || //capital letter
                (i >= 97 && i <= 122) || //lowercase letter
                (i == 45) || // - (dash)
                (i == 95) || // _ (underscore)
                (i == 46) || // . (period)
                (i == 47) || // / (slash)
                (i == 126)) // ~ (tilde)
        {
            output += i;
        } else {
            // percent encode char
            output += fmt::format("%{:02X}", (uint8_t)i);
        }
    }
    output.shrink_to_fit();
    return output;
}

std::string percent_decode_path(std::string_view input) {
    std::string output;
    output.reserve(input.size());
    for (size_t i = 0; i < input.size(); ++i) {
        if (input[i] != '%') {
            output += input[i];
        } else {
            uint8_t result;
            std::from_chars(&input[i + 1], &input[i + 3], result, 16);
            output += (char)result;
            i += 2; // skip hex
        }
    }
    output.shrink_to_fit();
    return output;
}

URIBuilder::URIBuilder(std::string_view scheme, std::string_view path) :
        scheme_(scheme),
        path_(path),
        encoded_path_(false)
{
}

URIBuilder &URIBuilder::encoded_path() {
    encoded_path_ = true;
    return *this;
}

URI URIBuilder::build() {
    std::string uri = std::string(scheme_) + ":";
    std::string enc_path;
    if (encoded_path_) {
        enc_path = path_;
    } else {
        enc_path = percent_encode_path(path_);
    }

    if (!authority_.empty()) {
        uri += ("//" + std::string(authority_));
        if (!enc_path.empty() && enc_path[0] != '/') {
            throw URIException("URI Path must be empty or start with '/' if authority is present");
        }
    }

    uri += enc_path;
    return URI(uri);
}


}
